'use strict';

const Object = require('../models/object')
const User = require('../models/user')
const utils = require('./utils');
const Boom = require('boom');

function Deg2Rad( deg ) {
  return deg * Math.PI / 180;
}

function PythagorasEquirectangular( lat1, lon1, lat2, lon2 ) {
  lat1 = Deg2Rad(lat1);
  lat2 = Deg2Rad(lat2);
  lon1 = Deg2Rad(lon1);
  lon2 = Deg2Rad(lon2);
  var R = 6371; // km
  var x = (lon2-lon1) * Math.cos((lat1+lat2)/2);
  var y = (lat2-lat1);
  var d = Math.sqrt(x*x + y*y) * R;
  return d;
}

Array.prototype.removeIf = function(callback) {
  var i = this.length;
  while (i--) {
    if (callback(this[i], i)) {
      this.splice(i, 1);
    }
  }
};

exports.find = {

  auth: false,

  handler: function (request, reply) {
    Object.find({}).populate('Provider_ID').exec().then(object => {
      reply(object);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};

exports.findObjectOfCategory = {

  auth: false,

  handler: function (request, reply) {
    var objects = [];
    var category = request.params.category.replace("_", " ");
    console.log('looking for ' + category);
    Object.find({ Category_1: category }).populate('Provider_ID').exec().then(object => {
      objects = objects.concat(object);
      return Object.find({ Category_2: category }).populate('Provider_ID').exec();
  }).then(object => {
      objects = objects.concat(object);
    return Object.find({ Category_3: category }).populate('Provider_ID').exec();
  }).then(object => {
    objects = objects.concat(object);
    reply(objects);
  }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
  });
  },

};

exports.findObjectOfProvider = {

  auth: false,

  handler: function (request, reply) {
    var objects = [];
    User.findOne({ UserNr: request.params.id }).exec().then(user => {
      return Object.find({ Provider_ID: user._id }).populate('Provider_ID').exec();
    }).then(object => {
      reply(object);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
  });
  },

};

exports.findObjectInRegion = {

  auth: false,

  handler: function (request, reply) {
    var objects = [];
    var user_lat1 = request.params.lat1;
    var user_lat2 = request.params.lat2;
    var user_lat = "";
    user_lat += user_lat1;
    user_lat += ".";
    user_lat += user_lat2;
    var user_long1 = request.params.long1;
    var user_long2 = request.params.long2;
    var user_long = "";
    user_long += user_long1;
    user_long += ".";
    user_long += user_long2;

    console.log(user_lat + ' ' + user_long);

    Object.find({}).exec().then(object => {
      object.removeIf( function(item, idx) {
        return PythagorasEquirectangular(item.Latitude, item.Longitude, parseFloat(user_lat), parseFloat(user_long)) > request.params.maxkm;
      });
      object.forEach(function(element) {
        element.tempDistance = PythagorasEquirectangular(element.Latitude, element.Longitude, parseFloat(user_lat), parseFloat(user_long));
      })
      object.sort(function(a,b) {return (a.tempDistance > b.tempDistance) ? 1 : ((a.tempDistance < b.tempDistance) ? -1 : 0);} );
      reply(object);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};

exports.findSpecific = {

  auth: false,

  handler: function (request, reply) {
    var target_lat = 0.0;
    var target_long = 0.0;
    Object.findOne({ Hopper_volume: 5000 }).exec().then(target => {
      target_lat = target.latitude;
      target_long = target.longitude;
      return Object.findOne({ Hopper_volume: request.params.hopper }).exec();
    }).then(object => {
      console.log(PythagorasEquirectangular(target_lat, target_long, object.latitude, object.longitude));
      reply(object);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};

exports.findOfUser = {

  auth: false,

  handler: function (request, reply) {
    Object.findOne({ owner: request.params.id }).then(objects => {
      reply(objects);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};
//
// exports.create = {
//
//   auth: false,
//   // auth: {
//   //   strategy: 'jwt',
//   // },
//
//   handler: function (request, reply) {
//
//     const tweet = new Tweet(request.payload);
//     tweet.creator = utils.getUserIdFromRequest(request);
//     tweet.date = new Date();
//
//     tweet.save().then(newTweet => {
//       Tweet.findOne(newTweet).populate('creator').then(tweet =>{
//         reply(tweet).code(201);
//       })
//     }).catch(err => {
//       reply(Boom.badImplementation('error making tweet'));
//     });
//   },
//
// };
//
// exports.deleteSpecific = {
//
//   handler: function (request, reply) {
//     Tweet.findByIdAndRemove({ tweet: request.params.id }).then(result => {
//       console.log('deleted tweet');
//       reply('/home');
//     }).catch(err => {
//       reply(Boom.badImplementation('error removing tweet'));
//     });
//   },
// };