const mongoose = require('mongoose');

const objectSchema = mongoose.Schema({
  Name_Product: String,
  Provider_ID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  Zip_code: Number,
  Address: String,
  Town: String,
  Product: String,
  Price: Number,
  Category_1: String,
  Category_2: String,
  Category_3: String,
  HP: Number,
  Model_year: Number,
  Operating_hours: Number,
  Brand: String,
  Model: String,
  Drive: String,
  ISO_BUS: Boolean,
  Tyre_size: String,
  Hopper_volume: Number,
  Weight: Number,
  Cylinder: Number,
  Energy: String,
  Gearbox: String,
  Sword_length: Number,
  Hitch: String,
  Working_width: Number,
  Description: String,
  Longitude: Number,
  Latitude: Number,
  available: Boolean,
  tempDistance: Number,
});

const Object = mongoose.model('Object', objectSchema);
module.exports = Object;