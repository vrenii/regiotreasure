'use strict';

const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  UserNr: Number,
  FirstName: String,
  LastName: String,
  Email: String,
  Address: String,
  Zip_code: Number,
  Town: String,
  Password: String,
  Image: String,
});

const User = mongoose.model('User', userSchema);
module.exports = User;