const UsersApi = require('./app/api/userapi');
const ObjectApi = require('./app/api/objectapi');

module.exports = [
  { method: 'GET', path: '/api/object/', config: ObjectApi.find },
  { method: 'GET', path: '/api/object/{id}', config: ObjectApi.findSpecific },

  { method: 'GET', path: '/api/object/category/{category}', config: ObjectApi.findObjectOfCategory },
  { method: 'GET', path: '/api/object/provider_id/{id}', config: ObjectApi.findObjectOfProvider },
  { method: 'GET', path: '/api/object/position/{lat1}/{lat2}/{long1}/{long2}/{maxkm}', config: ObjectApi.findObjectInRegion },

  { method: 'GET', path: '/api/users', config: UsersApi.find },
  { method: 'POST', path: '/api/users', config: UsersApi.create },
  { method: 'PUT', path: '/api/users', config: UsersApi.updateSettings },
  { method: 'POST', path: '/api/users/authenticate', config: UsersApi.authenticate },
];