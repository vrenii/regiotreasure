package com.hackaburgapp.verenaschmoeller.hackaburgapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class OverviewProjectAdapter extends BaseAdapter {
    Context context;
    List<ProjectItem> projectItems;
    LayoutInflater inflater;

    public OverviewProjectAdapter(Context applicationContext, List<ProjectItem> projectItems) {
        this.context = context;
        this.projectItems = projectItems;
        inflater = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return projectItems.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflater.inflate(R.layout.overview_list_item, null);
        TextView item = (TextView) view.findViewById(R.id.item);
        TextView info = (TextView) view.findViewById(R.id.info);
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView email = (TextView) view.findViewById(R.id.email);
        TextView phone = (TextView) view.findViewById(R.id.phone);
        item.setText(projectItems.get(i).getProjectName());
        info.setText(projectItems.get(i).getProjectInfo());
        name.setText("Date: "+projectItems.get(i).getStartDate().getDate()+"."+projectItems.get(i).getStartDate().getMonth()+"."+projectItems.get(i).getStartDate().getYear()+"-"+projectItems.get(i).getEndDate().getDate()+"."+projectItems.get(i).getEndDate().getMonth()+"."+projectItems.get(i).getEndDate().getYear());
        email.setText("Nr. workers: "+projectItems.get(i).getNumWorkers());
        String m="";
        for(int x=0;x<projectItems.get(i).getMachinery().size();x++){
            m=m+projectItems.get(i).getMachinery().get(x)+" ";
        }
        phone.setText(m);
        return view;
    }
}
