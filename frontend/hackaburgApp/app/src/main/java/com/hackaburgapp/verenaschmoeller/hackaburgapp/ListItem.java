package com.hackaburgapp.verenaschmoeller.hackaburgapp;

public class ListItem {

    private String item;
    private String info;
    private String name;
    private String address;
    private String phone;
    private String email;
    private double longitude;
    private double latitude;
    private String categroy;

    public ListItem(String item, String info, String name, String address, String phone, String email, double longitude, double latitude, String categroy) {
        this.item = item;
        this.info = info;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.longitude = longitude;
        this.latitude = latitude;
        this.categroy = categroy;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getCategroy() {
        return categroy;
    }

    public void setCategroy(String categroy) {
        this.categroy = categroy;
    }
}
