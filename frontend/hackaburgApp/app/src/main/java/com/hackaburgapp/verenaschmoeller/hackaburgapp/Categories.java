package com.hackaburgapp.verenaschmoeller.hackaburgapp;

import java.lang.String;

public class Categories {
    Categories[] subcategories;
    String name;
    String search_name;

    public Categories ( String name_,String search_name_,Categories[] subcategories_){
        name=name_;
        search_name=search_name_;
        subcategories=subcategories_;
    }
    public Categories[] getSubcategories(){
        return subcategories;
    }
    public String getName(){
        return name;
    }
}
