package com.hackaburgapp.verenaschmoeller.hackaburgapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class StartActivity extends AppCompatActivity {
    private static final Type PRODUCT_TYPE = new TypeToken<List<Product>>() {
    }.getType();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String path = "C:/Users/Sebastian/Desktop/data.json";

        Gson gson = new Gson();
        List<Product> data = new ArrayList<Product>();
        try {
            JsonReader reader = new JsonReader(new FileReader(path));
            data = gson.fromJson(reader, PRODUCT_TYPE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for(Product x: data){
            System.out.println(x.toString());
        }

        Button projectView = (Button) findViewById(R.id.projectView);
        projectView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), ProjectActivity.class);
                startActivityForResult(myIntent, 0);
            }

        });

        Button listView = (Button) findViewById(R.id.listView);
        listView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), ListActivity.class);
                startActivityForResult(myIntent, 0);
            }

        });

        Button mapView = (Button) findViewById(R.id.mapView);
        mapView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), MapsActivity.class);
                startActivityForResult(myIntent, 0);
            }

        });

        Button next = (Button) findViewById(R.id.register);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), RegisterActivity.class);
                startActivityForResult(myIntent, 0);

            }

        });



    }
}
