package com.hackaburgapp.verenaschmoeller.hackaburgapp;

public class Product {

    public String Name_Product;
    public Provider_ID provider_ID;
    public long Zip_code;
    public String Address;
    public String Town;
    public long Price;
    public String Category_1;
    public String Category_2;
    public String Category_3;
    public int HP;
    public String Email;
    public String Phone;
    //    public int Model_year;
    public long Operating_hours;
    //    public String Brand;
//    public String Model;
//    public String Drive;
//    public Boolean ISO_BUS;
//    public String Tyre_size;
//    public int Hopper_volume;
//    public int Weight;
//    public String Cylinder;
//    public String Energy;
//    public String Gearbox;
//    public int Sword_length;
//    public String Hitch;
//    public int Working_width;
    public String Description;
    public double Longitude;
    public double Latitude;

    public Product() {
        Name_Product = "";
        provider_ID = new Provider_ID();
//        Zip_code = 99999;
//        Address = "";
//        Town = "";
//        Price = 0;
//        Category_1 = "";
//        Category_2 = "";
//        Category_3 = "";
//        HP = 0;
//        Model_year = 0;
//        Operating_hours = 0;
//        Brand = "";
//        Model = "";
//        Drive = "";
//        ISO_BUS = true;
//        Tyre_size = "";
//        Hopper_volume = 0;
//        Weight = 0;
//        Cylinder = "";
//        Energy = "";
//        Gearbox = "";
//        Sword_length = 0;
//        Hitch = "";
//        Working_width = 0;
        Description = "";
        Longitude = 0.0;
        Latitude = 0.0;
    }

    public Product(String name_Product, Provider_ID provider_ID, long zip_code, String address, String town, long price, String category_1, String category_2, String category_3, int HP, int model_year, long operating_hours, String brand, String model, String drive, Boolean ISO_BUS, String tyre_size, int hopper_volume, int weight, String cylinder, String energy, String gearbox, int sword_length, String hitch, int working_width, String description, long longitude, long latitude) {
        Name_Product = name_Product;
        this.provider_ID = provider_ID;
//        Zip_code = zip_code;
//        Address = address;
//        Town = town;
//        Price = price;
//        Category_1 = category_1;
//        Category_2 = category_2;
//        Category_3 = category_3;
//        this.HP = HP;
//        Model_year = model_year;
//        Operating_hours = operating_hours;
//        Brand = brand;
//        Model = model;
//        Drive = drive;
//        this.ISO_BUS = ISO_BUS;
//        Tyre_size = tyre_size;
//        Hopper_volume = hopper_volume;
//        Weight = weight;
//        Cylinder = cylinder;
//        Energy = energy;
//        Gearbox = gearbox;
//        Sword_length = sword_length;
//        Hitch = hitch;
//        Working_width = working_width;
        Description = description;
        Longitude = longitude;
        Latitude = latitude;
    }

    @Override
    public String toString() {
        return "Product{" +
                "Name_Product='" + Name_Product + '\'' +
                ", provider_ID=" + provider_ID +
                ", Zip_code=" + Zip_code +
                ", Address='" + Address + '\'' +
                ", Town='" + Town + '\'' +
                ", Price=" + Price +
                ", Category_1='" + Category_1 + '\'' +
                ", Category_2='" + Category_2 + '\'' +
                ", Category_3='" + Category_3 + '\'' +
                ", HP=" + HP +
                ", Operating_hours=" + Operating_hours +
                ", Description='" + Description + '\'' +
                ", Longitude=" + Longitude +
                ", Latitude=" + Latitude +
                '}';
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public void setOperating_hours(long operating_hours) {
        Operating_hours = operating_hours;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public String getName_Product() {
        return Name_Product;
    }

    public void setName_Product(String name_Product) {
        Name_Product = name_Product;
    }

    public Provider_ID getProvider_ID() {
        return provider_ID;
    }

    public void setProvider_ID(Provider_ID provider_ID) {
        this.provider_ID = provider_ID;
    }

    public long getZip_code() {
        return Zip_code;
    }

    public void setZip_code(long zip_code) {
        Zip_code = zip_code;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getTown() {
        return Town;
    }

    public void setTown(String town) {
        Town = town;
    }

    public long getPrice() {
        return Price;
    }

    public void setPrice(long price) {
        Price = price;
    }

    public String getCategory_1() {
        return Category_1;
    }

    public void setCategory_1(String category_1) {
        Category_1 = category_1;
    }

    public String getCategory_2() {
        return Category_2;
    }

    public void setCategory_2(String category_2) {
        Category_2 = category_2;
    }

    public String getCategory_3() {
        return Category_3;
    }

    public void setCategory_3(String category_3) {
        Category_3 = category_3;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }
    //
//    public int getModel_year() {
//        return Model_year;
//    }
//
//    public void setModel_year(int model_year) {
//        Model_year = model_year;
//    }
//
    public long getOperating_hours() {
        return Operating_hours;
    }
    //
//    public void setOperating_hours(long operating_hours) {
//        Operating_hours = operating_hours;
//    }
//
//    public String getBrand() {
//        return Brand;
//    }
//
//    public void setBrand(String brand) {
//        Brand = brand;
//    }
//
//    public String getModel() {
//        return Model;
//    }
//
//    public void setModel(String model) {
//        Model = model;
//    }
//
//    public String getDrive() {
//        return Drive;
//    }
//
//    public void setDrive(String drive) {
//        Drive = drive;
//    }
//
//    public Boolean getISO_BUS() {
//        return ISO_BUS;
//    }
//
//    public void setISO_BUS(Boolean ISO_BUS) {
//        this.ISO_BUS = ISO_BUS;
//    }
//
//    public String getTyre_size() {
//        return Tyre_size;
//    }
//
//    public void setTyre_size(String tyre_size) {
//        Tyre_size = tyre_size;
//    }
//
//    public int getHopper_volume() {
//        return Hopper_volume;
//    }
//
//    public void setHopper_volume(int hopper_volume) {
//        Hopper_volume = hopper_volume;
//    }
//
//    public int getWeight() {
//        return Weight;
//    }
//
//    public void setWeight(int weight) {
//        Weight = weight;
//    }
//
//    public String getCylinder() {
//        return Cylinder;
//    }
//
//    public void setCylinder(String cylinder) {
//        Cylinder = cylinder;
//    }
//
//    public String getEnergy() {
//        return Energy;
//    }
//
//    public void setEnergy(String energy) {
//        Energy = energy;
//    }
//
//    public String getGearbox() {
//        return Gearbox;
//    }
//
//    public void setGearbox(String gearbox) {
//        Gearbox = gearbox;
//    }
//
//    public int getSword_length() {
//        return Sword_length;
//    }
//
//    public void setSword_length(int sword_length) {
//        Sword_length = sword_length;
//    }
//
//    public String getHitch() {
//        return Hitch;
//    }
//
//    public void setHitch(String hitch) {
//        Hitch = hitch;
//    }
//
//    public int getWorking_width() {
//        return Working_width;
//    }
//
//    public void setWorking_width(int working_width) {
//        Working_width = working_width;
//    }
//
    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(long longitude) {
        Longitude = longitude;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(long latitude) {
        Latitude = latitude;
    }
}
