package com.hackaburgapp.verenaschmoeller.hackaburgapp;

import java.util.Date;
import java.util.List;

public class ProjectItem {

    private String projectName;
    private String projectInfo;
    private Date startDate;
    private Date endDate;
    private int numWorkers;
    private List<String> machinery;
    private double longitude;
    private double latitude;

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public ProjectItem(String projectName, String projectInfo, Date startDate, Date endDate, int numWorkers, List<String> machinery, double longitude, double latitude) {
        this.projectName = projectName;
        this.projectInfo = projectInfo;
        this.startDate = startDate;
        this.endDate = endDate;
        this.numWorkers = numWorkers;
        this.machinery = machinery;
        this.longitude = longitude;
        this.latitude = latitude;
    }



    public String getProjectName() {
        return projectName;
    }

    public String getProjectInfo() {
        return projectInfo;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public int getNumWorkers() {
        return numWorkers;
    }

    public List<String> getMachinery() {
        return machinery;
    }
}
