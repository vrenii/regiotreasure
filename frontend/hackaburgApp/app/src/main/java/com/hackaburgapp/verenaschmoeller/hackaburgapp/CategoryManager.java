package com.hackaburgapp.verenaschmoeller.hackaburgapp;

public class CategoryManager {
    Categories categories_tree;
    public CategoryManager(){
        Categories disk_harrer= new Categories("Disk harrer","Disk_harrer",null);
        Categories power_harrer=new Categories("Power harrer","Power_harrer",null);
        Categories plough= new Categories("Plough","Plough",null);
        Categories [] soil_cultivation_arr = {disk_harrer,power_harrer,plough};
        Categories soil_cultivation= new Categories("Soil cultivation","Soil_cultivation",soil_cultivation_arr);
        Categories seeders=new Categories("Seeder","Seeder",null);
        Categories sprayers=new Categories("Sprayers","Sprayers",null);
        Categories planters=new Categories("Planters","Planters", null);
        Categories [] accessory_equipment_arr= {soil_cultivation,seeders,sprayers,planters};
        Categories accessory_equipment=new Categories("Accessory equipment","Accessory_equipment",accessory_equipment_arr);
        Categories motor_tractor= new Categories("Motor tractor","Motor_tractor",null);
        Categories wood=new Categories("Wood","Wood",null);
        Categories metal= new Categories("Metal","Metal",null);
        Categories stone=new Categories("Stone", "Stone",null);
        Categories [] tool_arr={wood,metal,stone};
        Categories tools = new Categories("Tools","Tools",tool_arr);
        Categories field=new Categories("Field","Field",null);
        Categories construction=new Categories("Construction","Construction",null);
        Categories [] service_arr={tools,field,construction};
        Categories service=new Categories("Service","Service",service_arr);
        Categories []root_arr={accessory_equipment,motor_tractor,tools,service};
        categories_tree=new Categories("Root","Root",root_arr);
    }

    public Categories[] getFirstLevel(){

        return  categories_tree.getSubcategories();
    }

    public Categories[] getSecondLevel(String first_level){
        Categories[] first_l=getFirstLevel();
        for(int i = 0;i<first_l.length;i++){
            if(first_l[i].getName().equals(first_level)){
                return first_l[i].getSubcategories();
            }
        }
        return null;
    }

    public Categories[] getThirdLevel(String frist_level,String second_level){
        Categories[] second_l=getSecondLevel(frist_level);
        if(second_l!=null) {
            for (int i = 0; i < second_l.length; i++) {
                if (second_l[i].getName().equals(second_level)) {
                    return second_l[i].getSubcategories();
                }
            }
        }
        return null;
    }
}
