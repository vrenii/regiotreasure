package com.hackaburgapp.verenaschmoeller.hackaburgapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class OverviewListAdapter extends BaseAdapter {
    Context context;
    List<ListItem> listItems;
    LayoutInflater inflater;

    public OverviewListAdapter(Context applicationContext, List<ListItem> listItems) {
        this.context = context;
        this.listItems = listItems;
        inflater = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.overview_list_item, null);
        TextView item = (TextView) view.findViewById(R.id.item);
        TextView info = (TextView) view.findViewById(R.id.info);
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView email = (TextView) view.findViewById(R.id.email);
        TextView phone = (TextView) view.findViewById(R.id.phone);
        item.setText(listItems.get(i).getItem());
        info.setText(listItems.get(i).getInfo());
        name.setText(listItems.get(i).getName());
        email.setText(listItems.get(i).getEmail());
        phone.setText(listItems.get(i).getPhone());
        return view;
    }
}
