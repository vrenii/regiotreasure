package com.hackaburgapp.verenaschmoeller.hackaburgapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class CategoriesPopup extends AppCompatActivity {

    private static final String TAG = "MyActivity";
    private ListView mainListView ;
    private ArrayAdapter listAdapter ;
    private List<String> firstLevelCategories ;
    private List<Boolean> firstLevelCategoriesClicked;
    private List<String> secondLevelCatagories;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories_popup);

        // Find the ListView resource.
        mainListView = (ListView) findViewById( R.id.categoryList );
        final CategoryManager categories = new CategoryManager();

        Categories [] firstLevel = categories.getFirstLevel();
        firstLevelCategories = new ArrayList<String>();
        firstLevelCategoriesClicked = new ArrayList<Boolean>();
        for(int i = 0; i < firstLevel.length; i++) {
            firstLevelCategories.add(firstLevel[i].name);
            firstLevelCategoriesClicked.add(false);
            if (firstLevel[i].getSubcategories() != null) {
                Categories[] secondLevel = firstLevel[i].getSubcategories();

            }
        }
        listAdapter = new ArrayAdapter<String>(this, R.layout.list_item, firstLevelCategories);
        // setting list adapter
        mainListView.setAdapter(listAdapter);

        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG,"on click: " + parent.getItemAtPosition(position));
                if(categories.getSecondLevel(parent.getItemAtPosition(position).toString())!=null) {
                    Categories[] c = categories.getSecondLevel(parent.getItemAtPosition(position).toString());

                    if (!firstLevelCategoriesClicked.get(position)) {
                        firstLevelCategoriesClicked.set(position, true);

                        for (int i = 0; i < c.length; i++) {
                            firstLevelCategories.add(position + i + 1, "- " + c[i].getName());
                            firstLevelCategoriesClicked.add(position + i + 1,false);
                            Log.e(TAG, c[i].getName());
                        }




                    }else {
                        firstLevelCategoriesClicked.set(position,false);
                        for (int i = 0; i < c.length; i++) {
                            firstLevelCategories.remove(position +  1);
                            firstLevelCategoriesClicked.remove(position + 1);
                        }
                    }
                    listAdapter.notifyDataSetChanged();
                }
            }
        });
    }

}
