package com.hackaburgapp.verenaschmoeller.hackaburgapp;

import java.util.ArrayList;
import java.util.List;

public class ListManager {
    private List<ListItem> list = new ArrayList();

    public ListManager(){
        list.add(new ListItem("Horsch Pronto", "Drilling wheat with high speed up to 20 km/h", "Johann Meierl", "Edlingerstrasse 2, 93053 Regensburg", "015158368291", "johann@meier.com", 49.06782190851479, 12.089642421558125, "Accessory equipment"));
        list.add(new ListItem("Fendt 718", "Perfect for transport ", "Johann Meierl", "Hemauerstraße 12, 93047 Regensburg", "015158368291", "johann@meier.com", 48.87411860825185, 12.05771981690104, "Motor Tractor"));
        list.add(new ListItem("Chain Saw ", "Good saw for tree cutting", "Johann Meierl", "Villagestreet 3, 93049 Regensburg", "015158368291", "johann@meier.com", 48.97333614362189, 12.064309809276411, "Tools"));
        list.add(new ListItem("Driver", "I have a lot experience in combines and tractors. Working without limit. ", "Johann Meierl", "Agristree 3, 93058 Regensburg", "015158368291", "johann@meier.com", 50.13271696704674, 12.178334591169003, "Service"));
        list.add(new ListItem("Horsch Pronto 7", "Drilling wheat with high speed up to 20 km/h", "Johann Meierl", "Edlingerstrasse 2, 93053 Regensburg", "015158368291", "johann@meier.com", 49.12196032960024, 12.31477841234645, "Accessory equipment"));
        list.add(new ListItem("Fendt 718 7", "Perfect for transport ", "Johann Meierl", "Hemauerstraße 12, 93047 Regensburg", "015158368291", "johann@meier.com", 49.248163094689986, 12.398694016613033, "Motor Tractor"));
        list.add(new ListItem("Chain Saw 7", "Good saw for tree cutting", "Johann Meierl", "Villagestreet 3, 93049 Regensburg", "015158368291", "johann@meier.com", 49.14525186206636, 12.2118651896603, "Tools"));
        list.add(new ListItem("Driver 7", "I have a lot experience in combines and tractors. Working without limit. ", "Johann Meierl", "Agristree 3, 93058 Regensburg", "015158368291", "johann@meier.com", 49.093349892532615, 12.133256068030123, "Service"));
        list.add(new ListItem("Horsch Pronto 6", "Drilling wheat with high speed up to 20 km/h", "Johann Meierl", "Edlingerstrasse 2, 93053 Regensburg", "015158368291", "johann@meier.com", 49.07780298478601, 12.128105356804765, "Accessory equipment"));
        list.add(new ListItem("Fendt 718 6", "Perfect for transport ", "Johann Meierl", "Hemauerstraße 12, 93047 Regensburg", "015158368291", "johann@meier.com", 48.89546280211999, 11.957999098162269, "Motor Tractor"));
        list.add(new ListItem("Chain Saw 6", "Good saw for tree cutting", "Johann Meierl", "Villagestreet 3, 93049 Regensburg", "015158368291", "johann@meier.com", 49.00782781186033, 13.014915854690068, "Tools"));
        list.add(new ListItem("Driver 6", "I have a lot experience in combines and tractors. Working without limit. ", "Johann Meierl", "Agristree 3, 93058 Regensburg", "015158368291", "johann@meier.com", 48.76736621028744, 12.824626648195883, "Service"));
        list.add(new ListItem("Horsch Pronto 5", "Drilling wheat with high speed up to 20 km/h", "Johann Meierl", "Edlingerstrasse 2, 93053 Regensburg", "015158368291", "johann@meier.com", 48.9727846971106, 12.053392148559285, "Accessory equipment"));
        list.add(new ListItem("Fendt 718 5", "Perfect for transport ", "Johann Meierl", "Hemauerstraße 12, 93047 Regensburg", "015158368291", "johann@meier.com", 49.136899590242734, 12.23106281124421, "Motor Tractor"));
        list.add(new ListItem("Chain Saw 5", "Good saw for tree cutting", "Johann Meierl", "Villagestreet 3, 93049 Regensburg", "015158368291", "johann@meier.com", 49.1086738580362, 12.160319742190927, "Tools"));
        list.add(new ListItem("Driver 5", "I have a lot experience in combines and tractors. Working without limit. ", "Johann Meierl", "Agristree 3, 93058 Regensburg", "015158368291", "johann@meier.com", 49.90847542393848, 12.067730845077206, "Service"));
        list.add(new ListItem("Horsch Pronto 4", "Drilling wheat with high speed up to 20 km/h", "Johann Meierl", "Edlingerstrasse 2, 93053 Regensburg", "015158368291", "johann@meier.com", 49.01914895840142, 12.125669257440476, "Accessory equipment"));
        list.add(new ListItem("Fendt 718 4", "Perfect for transport ", "Johann Meierl", "Hemauerstraße 12, 93047 Regensburg", "015158368291", "johann@meier.com", 49.141986076045015, 12.122193818394374, "Motor Tractor"));
        list.add(new ListItem("Chain Saw 4", "Good saw for tree cutting", "Johann Meierl", "Villagestreet 3, 93049 Regensburg", "015158368291", "johann@meier.com", 49.05933451798388, 12.150579824801868, "Tools"));
        list.add(new ListItem("Driver 4", "I have a lot experience in combines and tractors. Working without limit. ", "Johann Meierl", "Agristree 3, 93058 Regensburg", "015158368291", "johann@meier.com", 48.97543189977636, 12.069685744114071, "Service"));
        list.add(new ListItem("Horsch Pronto 3", "Drilling wheat with high speed up to 20 km/h", "Johann Meierl", "Edlingerstrasse 2, 93053 Regensburg", "015158368291", "johann@meier.com", 48.92367834794226, 12.037513585534914, "Accessory equipment"));
        list.add(new ListItem("Fendt 718 3", "Perfect for transport ", "Johann Meierl", "Hemauerstraße 12, 93047 Regensburg", "015158368291", "johann@meier.com", 48.930927539396095, 12.017555032904912, "Motor Tractor"));
        list.add(new ListItem("Chain Saw 3", "Good saw for tree cutting", "Johann Meierl", "Villagestreet 3, 93049 Regensburg", "015158368291", "johann@meier.com", 49.14213153382911, 12.183386027739434, "Tools"));
        list.add(new ListItem("Driver 3", "I have a lot experience in combines and tractors. Working without limit. ", "Johann Meierl", "Agristree 3, 93058 Regensburg", "015158368291", "johann@meier.com", 48.86688869085397, 11.979820763103561, "Service"));
        list.add(new ListItem("Horsch Pronto 2", "Drilling wheat with high speed up to 20 km/h", "Johann Meierl", "Edlingerstrasse 2, 93053 Regensburg", "015158368291", "johann@meier.com", 48.95703094126801, 11.9636979778816, "Accessory equipment"));
        list.add(new ListItem("Fendt 718 2", "Perfect for transport ", "Johann Meierl", "Hemauerstraße 12, 93047 Regensburg", "015158368291", "johann@meier.com", 49.15162079912578, 12.158009848736471, "Motor Tractor"));
        list.add(new ListItem("Chain Saw 2", "Good saw for tree cutting", "Johann Meierl", "Villagestreet 3, 93049 Regensburg", "015158368291", "johann@meier.com", 49.022619935699424, 12.13538447357776, "Tools"));
        list.add(new ListItem("Driver 2", "I have a lot experience in combines and tractors. Working without limit. ", "Johann Meierl", "Agristree 3, 93058 Regensburg", "015158368291", "johann@meier.com", 49.06217959007857, 12.140239883299824, "Service"));
        list.add(new ListItem("Horsch Pronto 1", "Drilling wheat with high speed up to 20 km/h", "Johann Meierl", "Edlingerstrasse 2, 93053 Regensburg", "015158368291", "johann@meier.com", 48.938834015997955, 11.940739269562727, "Accessory equipment"));
        list.add(new ListItem("Fendt 718 1", "Perfect for transport ", "Johann Meierl", "Hemauerstraße 12, 93047 Regensburg", "015158368291", "johann@meier.com", 49.03340982991108, 12.222014841993241, "Motor Tractor"));
        list.add(new ListItem("Chain Saw 1", "Good saw for tree cutting", "Johann Meierl", "Villagestreet 3, 93049 Regensburg", "015158368291", "johann@meier.com", 49.09729225610739, 12.171137400846945, "Tools"));
        list.add(new ListItem("Driver 1", "I have a lot experience in combines and tractors. Working without limit. ", "Johann Meierl", "Agristree 3, 93058 Regensburg", "015158368291", "johann@meier.com", 49.97441367344466, 12.086443727232522, "Service"));

    }

    public List getData(){
        return list;
    }

}
