package com.hackaburgapp.verenaschmoeller.hackaburgapp;

public class Provider_ID {
    public String FirstName;
    public String LastName;
    public String Email;

    public Provider_ID() {
        FirstName = "";
        LastName = "";
        Email = "";

    }

    public Provider_ID(String firstName, String lastName, String email) {
        FirstName = firstName;
        LastName = lastName;
        Email = email;
    }

    @Override
    public String toString() {
        return "Provider_ID{" +
                "FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", Email='" + Email + '\'' +
                '}';
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}
