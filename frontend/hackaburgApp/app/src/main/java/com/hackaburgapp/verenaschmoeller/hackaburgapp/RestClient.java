package com.hackaburgapp.verenaschmoeller.hackaburgapp;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

//public class RestClient extends AsyncTask<String, Void, String> {

//    @Override
//    protected String doInBackground(String... strings) {
//        return getData();
//    }
//
//    protected void onPreExecute(){
//
//    }

public class RestClient implements Runnable{
    private static final Type PRODUCT_TYPE = new TypeToken<List<Product>>() {
    }.getType();

    public List<Product> getData() {
        List<Product> data = new ArrayList<Product>();
        try {
            URL url = new URL("https://vast-anchorage-21887.herokuapp.com/api/object/");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");

            //BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new InputStreamReader(connection.getInputStream()));
            data = gson.fromJson(reader, PRODUCT_TYPE); // contains the whole reviews list

            for(Product x: data) {
                System.out.println(x.toString());
            }

//            StringBuilder builder = new StringBuilder();
//            String line = null;
//            while ((line = br.readLine()) != null) {
//                builder.append(line);
//            }
//
//            return builder.toString();
            return data;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        getData();
    }
}
