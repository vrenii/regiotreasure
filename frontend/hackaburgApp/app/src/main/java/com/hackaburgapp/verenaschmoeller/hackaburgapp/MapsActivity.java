package com.hackaburgapp.verenaschmoeller.hackaburgapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private MapView mapView;
    private GoogleMap mMap;
    private DrawerLayout mDrawerLayout;
    private boolean onLoadMap = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        mDrawerLayout.addDrawerListener(
                new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Respond when the drawer is opened
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        // Respond when the drawer is closed
                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                    }
                }
        );

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        switch (menuItem.getItemId()) {
                            case R.id.categories:
                                Intent myIntent = new Intent(getApplicationContext(), CategoriesPopup.class);
                                startActivityForResult(myIntent, 0);
                        }

                        //Toast itemId = Toast.makeText(getApplicationContext(), menuItem.getItemId(), Toast.LENGTH_SHORT);
                        //itemId.show();

                        return true;
                    }
                });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        FloatingActionButton back = (FloatingActionButton) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), StartActivity.class);
                startActivityForResult(myIntent, 0);
            }

        });

        FloatingActionButton flyTo = (FloatingActionButton) findViewById(R.id.flyTo);
        flyTo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onLoadMap = true;
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        ListManager listManager = new ListManager();
        List<ListItem> list = listManager.getData();

        for (ListItem item : list) {

            if (item.getCategroy() == "Tools") {
                mMap.addMarker(new MarkerOptions().position(new LatLng(item.getLongitude(), item.getLatitude())).title(item.getItem()).snippet(item.getName() + ", " + item.getPhone()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.tools150)));
            }
            if (item.getCategroy() == "Accessory equipment") {
                mMap.addMarker(new MarkerOptions().position(new LatLng(item.getLongitude(), item.getLatitude())).title(item.getItem()).snippet(item.getName() + ", " + item.getPhone()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.plow150)));
            }
            if (item.getCategroy() == "Service") {
                mMap.addMarker(new MarkerOptions().position(new LatLng(item.getLongitude(), item.getLatitude())).title(item.getItem()).snippet(item.getName() + ", " + item.getPhone()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.farmer150)));
            }
            else{
                mMap.addMarker(new MarkerOptions().position(new LatLng(item.getLongitude(), item.getLatitude())).title(item.getItem()).snippet(item.getName() + ", " + item.getPhone()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.tractor150)));
            }
        }

        ProjectManager projectManager = new ProjectManager();
        List<ProjectItem> projectItems = projectManager.getProjectItems();

        for (ProjectItem item : projectItems){
            mMap.addMarker(new MarkerOptions().position(new LatLng(item.getLongitude(), item.getLatitude())).title(item.getProjectName()).snippet(item.getProjectInfo() + ", " + item.getStartDate()).icon(BitmapDescriptorFactory.fromResource(R.mipmap.product150)));
        }


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);


        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {

                if (onLoadMap) {
                    CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(11);
                    mMap.moveCamera(center);
                    mMap.animateCamera(zoom);
                    onLoadMap = false;
                }

            }
        });


    }

}
