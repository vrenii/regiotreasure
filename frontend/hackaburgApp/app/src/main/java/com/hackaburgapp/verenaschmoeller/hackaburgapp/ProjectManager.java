package com.hackaburgapp.verenaschmoeller.hackaburgapp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProjectManager {
    private List<ProjectItem> projectItems=new ArrayList<ProjectItem>();

    public ProjectManager(){
        List<String> machinery=new ArrayList<String>();
        machinery.add("Tractor");
        projectItems.add(new ProjectItem("Harvest","Harvest 3 hectare",new Date(2018,8,1),new Date(2018,8,3),5,machinery,48.1234,13.123));
        machinery.add("Seeder");
        projectItems.add(new ProjectItem("Seeding","Seed mid summer seeds", new Date(2018,9,13),new Date(2018,9,19),2,machinery,49.93412,12.4237));
        machinery.remove(1);
        projectItems.add(new ProjectItem("Spray manure","Spray 1 hectare", new Date(2018,10,21),new Date(2018,10,24),4,machinery,49.5654,12.1112));
        projectItems.add(new ProjectItem("Harvest","Harvest 3 hectare",new Date(2018,8,1),new Date(2018,8,3),5,machinery,49.01914,12.125));

    }

    public List<ProjectItem> getProjectItems() {
        return projectItems;
    }
}
